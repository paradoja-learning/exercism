class Bob
  def self.hey(phrase)
    empty    = :empty?.to_proc
    caps     = ->(string) { string == string.upcase && string != string.downcase }
    question = ->(string) { string.strip[-1] == '?' }
    shout_question = ->(string) { caps.call(string) && question.call(string) }

    case phrase.strip
    when shout_question then 'Calm down, I know what I\'m doing!'
    when empty    then 'Fine. Be that way!'
    when caps     then 'Whoa, chill out!'
    when question then 'Sure.'
    else               'Whatever.'
    end
  end
end
