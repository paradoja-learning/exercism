{
  description = "Exercism ruby";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let
          inherit (nixpkgs.lib) optional optionals;
          ruby = pkgs.ruby_3_1;
          rp = pkgs.rubyPackages_3_1;
          check = pkgs.writeShellScriptBin "check" ''
            if [[ ! -z $1 ]]; then cd $1; fi
            exec ${ruby}/bin/ruby -r minitest/pride *_test.rb
          '';
        in pkgs.mkShell {
          buildInputs = with pkgs; [
            ruby
            rp.solargraph
            rp.minitest
            bundler
            yaml-language-server

            check
            exercism
          ];
          shellHook = "";
        };
      });
}
