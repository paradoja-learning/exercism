defmodule Bob do
  @spec hey(String.t()) :: String.t()
  def hey(input) do
    i = String.trim(input)

    cond do
      i == "" -> "Fine. Be that way!"
      question?(i) && shouting?(i) -> "Calm down, I know what I'm doing!"
      question?(i) -> "Sure."
      shouting?(i) -> "Whoa, chill out!"
      true -> "Whatever."
    end
  end

  defp question?(input) do
    String.ends_with?(input, "?")
  end

  defp shouting?(input) do
    upcased = String.upcase(input)
    upcased == input and String.downcase(input) != upcased
  end
end
