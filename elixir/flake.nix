{
  description = "Elixir exercism";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let
          inherit (nixpkgs.lib) optional optionals;

          elixir = pkgs.beam.packages.erlangR24.elixir_1_14;
          nodejs = pkgs.nodejs-17_x;

          check = pkgs.writeShellScriptBin "check" ''
            exec ${elixir}/bin/mix test
          '';
          check-watch = pkgs.writeShellScriptBin "check-watch" ''
            exec ${pkgs.watchexec}/bin/watchexec -r -e ex,exs ${elixir}/bin/mix test
          '';
        in pkgs.mkShell {
          buildInputs = with pkgs; [
            # elixir
            elixir
            esbuild
            beamPackages.hex
            elixir_ls

            exercism
            check
            watchexec # don't want to add dependencies to mix manually if I can avoid it
            check-watch
          ];
          shellHook = "";
        };
      });
}
