defmodule WordCount do
  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t()) :: map
  def count(sentence) do
    Enum.reduce(words_from(sentence), %{}, &add_to_map/2)
  end

  defp words_from(sentence) do
    Regex.split(~r/([^'\w-]|_)+/u, String.trim(sentence))
  end

  defp add_to_map("", map) do
    map
  end

  defp add_to_map(word, map) do
    normalized_word = word |> String.downcase() |> String.replace(~r/^'(.+)'$/, "\\1")
    current_ammount = map[normalized_word] || 0
    Map.put(map, normalized_word, current_ammount + 1)
  end
end
