{
  description = "Exercism exercises";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        devTools = with pkgs; [
          exercism

          # bash & nix
          nodePackages.bash-language-server
          shellcheck
          nixfmt
          rnix-lsp
        ];

      in { devShells.default = pkgs.mkShell { buildInputs = devTools; }; });
}
