* Project Inbox
** DONE Check old Elm exercises
:LOGBOOK:
- State "DONE"       from "TODO"       [2023-02-01 Mi 10:33]
CREATED: [2023-01-31 Di 01:25]
:END:

[[orgit:~/projects/learning/exercism/][~/projects/learning/exercism/ (magit-status)]]
** DONE Check JS old exercise
:LOGBOOK:
- State "DONE"       from "TODO"       [2023-01-31 Di 19:35]
CREATED: [2023-01-31 Di 01:08]
:END:

[[file:~/projects/learning/exercism/javascript/flake.nix::nodejs]]
** DONE Check old elixir exercises
:LOGBOOK:
- State "DONE"       from "TODO"       [2023-01-31 Di 19:10]
CREATED: [2023-01-31 Di 01:25]
:END:
** DONE Old scala code
:LOGBOOK:
- State "DONE"       from "TODO"       [2023-01-31 Di 18:17]
CREATED: [2023-01-31 Di 00:31]
:END:
They were written a long time ago and probably fail new tests
** DONE Fix old Ruby exercises
:LOGBOOK:
- State "DONE"       from "TODO"       [2023-01-31 Di 16:30] \\
  Fixed bob. Rather uglily I'll say, but whatever.
CREATED: [2023-01-31 Di 00:30]
:END:
They fail current tests
