{
  description = "Emacs lisp";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShell =
          let
            inherit (nixpkgs.lib) optional optionals;
            check = pkgs.writeShellScriptBin "check" ''
              exec emacs -batch -l ert -l *-test.el -f ert-run-tests-batch-and-exit
            '';
          in
          pkgs.mkShell {
            buildInputs = with pkgs; [
              # emacs # - not installing as it's assumed installed and I don't want to config integration
              check
            ];
            shellHook = "";
          };
      });
}
