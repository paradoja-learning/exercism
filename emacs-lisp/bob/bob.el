;;; bob.el --- Bob exercise (exercism)

;;; Commentary:

;;; Code:

(string-match-p "\?[\s\t]*$" (replace-regexp-in-string "\n" " " "\nDoes this cryogenic chamber make me look fat?\nno"))

(defun response-for (phrase)
  (cl-flet* ((normalize (phrase)
                        (replace-regexp-in-string "^[\s\t\15\11]*" ""
                                                  (replace-regexp-in-string "\n" " " phrase)))
             (shoutingp (phrase)
                        (and (string= phrase (upcase phrase))
                             (not (string= phrase (downcase phrase)))))
             (questionp (phrase)
                        (string-match-p "\?[\s\t]*$"
                                        (normalize phrase)))
             (silencep (phrase)
                       (string-match-p "^[\s\t\n]*$" (normalize phrase))))
    (cond ((and (shoutingp phrase)
                (questionp phrase)) "Calm down, I know what I'm doing!")
          ((shoutingp phrase) "Whoa, chill out!")
          ((questionp phrase) "Sure.")
          ((silencep phrase) "Fine. Be that way!")
          (t "Whatever."))))

(provide 'bob)
;;; bob.el ends here
