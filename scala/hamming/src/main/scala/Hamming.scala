object Hamming {
  def distance(firstStrand: String, secondStrand: String): Option[Int] =
    auxCompute(firstStrand.toList, secondStrand.toList, 0)

  private def auxCompute(
      first: List[Char],
      second: List[Char],
      total: Int
  ): Option[Int] =
    (first, second) match {
      case (List(), List())             => Some(total)
      case (x :: xs, y :: ys) if x == y => auxCompute(xs, ys, total)
      case (x :: xs, y :: ys)           => auxCompute(xs, ys, total + 1)
      case _                            => None
    }
}
