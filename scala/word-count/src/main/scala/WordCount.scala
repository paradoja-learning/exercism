case class WordCount(string: String) {
  lazy val countWords: Map[String, Int] = {
    val words =
      "[^'\\w\"]+".r
        .replaceAllIn(string.trim, " ")
        .toLowerCase
        .split("\\s+")
        .toList
        .map("^'(.*)'$".r.replaceFirstIn(_, "$1"))

    for {
      (word, wordList) <- words.groupBy(identity)
    } yield (word, wordList.length)
  }
}
