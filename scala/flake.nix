{
  description = "Scala exercism";
  # Using https://github.com/typelevel/typelevel-nix

  inputs = {
    typelevel-nix.url = "github:typelevel/typelevel-nix";
    nixpkgs.follows = "typelevel-nix/nixpkgs";
    flake-utils.follows = "typelevel-nix/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, typelevel-nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ typelevel-nix.overlay ];
        };
        check = pkgs.writeShellScriptBin "check" ''
          exec ${pkgs.sbt}/bin/sbt test
        '';
        check-watch = pkgs.writeShellScriptBin "check-watch" ''
          exec ${pkgs.sbt}/bin/sbt "~ Test / test"
        '';
      in {
        devShell = pkgs.devshell.mkShell {
          imports = [ typelevel-nix.typelevelShell ];
          name = "scala-cats";
          packages = [ check check-watch pkgs.exercism ];
          typelevelShell = { jdk.package = pkgs.jdk17; };
        };
      });
}
