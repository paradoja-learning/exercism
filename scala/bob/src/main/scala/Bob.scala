object Bob {
  def response(phrase: String): String = phrase.trim match {
    case "" => "Fine. Be that way!"
    case p if isShouting(p) && isQuestion(p) =>
      "Calm down, I know what I'm doing!"
    case p if isShouting(p) => "Whoa, chill out!"
    case p if isQuestion(p) => "Sure."
    case _                  => "Whatever."
  }

  private def isShouting(phrase: String): Boolean =
    phrase.toUpperCase == phrase &&
      phrase.toLowerCase != phrase.toUpperCase

  private def isQuestion(phrase: String): Boolean = phrase.endsWith("?")
}
