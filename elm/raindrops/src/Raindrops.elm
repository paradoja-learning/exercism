module Raindrops exposing (raindrops)

import List
import String

ruleset = [(3, "Pling")
          ,(5, "Plang")
          ,(7, "Plong")]


raindrops : Int -> String
raindrops n =
    let ruleF = outputIfDivisible n
        divisorName = List.foldl (\rule acc -> acc ++ ruleF rule) "" ruleset
    in case divisorName of
           "" -> String.fromInt n
           _ -> divisorName

outputIfDivisible : Int -> (Int, String) -> String
outputIfDivisible n (divisor, output) =
    if remainderBy divisor n == 0 then output else ""
