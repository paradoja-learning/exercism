module Triangle exposing (Triangle(..), triangleKind)

import List

type Triangle = Isosceles
              | Equilateral
              | Scalene

satisfiesInequality : number -> number -> number -> Bool
satisfiesInequality x y z =
    case List.sort [x, y, z] of
        [a, b, c] -> a + b >= c
        _ -> False

triangleKind : number -> number -> number -> Result String Triangle
triangleKind x y z =
    if List.any (\n -> n <= 0) [x, y, z] then
        Err "Invalid lengths"
    else if not <| satisfiesInequality x y z then
        Err  "Violates inequality"
    else if x == y && y == z then
        Ok Equilateral
    else if x == y || x == z || y == z then
        Ok Isosceles
    else
        Ok Scalene
