{
  description = "Elm exercism";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let
          inherit (nixpkgs.lib) optional optionals;
          nodejs = pkgs.nodejs-17_x;
          elmPackages = pkgs.elmPackages;
          check = pkgs.writeShellScriptBin "check" ''
            exec ${elmPackages.elm-test}/bin/elm-test
          '';
          check-watch = pkgs.writeShellScriptBin "check-watch" ''
            exec ${elmPackages.elm-test}/bin/elm-test --watch
          '';
        in pkgs.mkShell {
          buildInputs = with pkgs;
            with elmPackages; [
              elm
              elm-test
              elm-language-server

              check
              check-watch
              exercism
            ];
          shellHook = "";
        };
      });
}
