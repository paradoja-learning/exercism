module Pangram exposing (isPangram)

import List
import Set
import String exposing (..)

letters : String
letters = "abcdefghijklmnopqrstuvwxyz"

isPangram : String -> Bool
isPangram =
    toLower >> filter isLetter >> toList >> removeDuplicates
        >> List.sort >> fromList >> (==) letters

listOfLetters : List Char
listOfLetters = toList letters

isLetter : Char -> Bool
isLetter =
    let member x y = List.member y x
    in member <| listOfLetters

removeDuplicates : List comparable -> List comparable
removeDuplicates = Set.fromList >> Set.toList
