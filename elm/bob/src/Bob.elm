module Bob exposing (hey)

import String

hey : String -> String
hey something =
    let phrase = String.trim something
    in if silent phrase then
           "Fine. Be that way!"
       else if shouting phrase && asking phrase then
           "Calm down, I know what I'm doing!"
       else if shouting phrase then
           "Whoa, chill out!"
       else if asking phrase then
                "Sure."
       else "Whatever."

silent : String -> Bool
silent = (==) ""

shouting : String -> Bool
shouting phrase = phrase == String.toUpper phrase &&
                  String.toLower phrase /= phrase

asking : String -> Bool
asking = String.endsWith "?"
