module Accumulate exposing (accumulate)


accumulate : (a -> b) -> List a -> List b
accumulate f list =
    case list of
        [] -> []
        x :: xs -> f x :: accumulate f xs
