{
  description = "Rust exercism";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let
          inherit (nixpkgs.lib) optional optionals;

          check = pkgs.writeShellScriptBin "check" ''
            if [[ ! -z $1 ]]; then cd $1; fi
            exec ${pkgs.cargo}/bin/cargo test
          '';
          check-watch = pkgs.writeShellScriptBin "check-watch" ''
            if [[ ! -z $1 ]]; then cd $1; fi
            exec ${pkgs.watchexec}/bin/watchexec -r -e rs ${pkgs.cargo}/bin/cargo test
          '';

        in pkgs.mkShell {
          buildInputs = with pkgs; [
            rustc
            rust.packages.stable.rustPlatform.rustcSrc # rust-src
            cargo
            gcc
            rustfmt
            clippy
            rust-analyzer
            pkg-config

            # other build tools
            openssl

            watchexec
            check
            check-watch
            exercism
          ];
          RUST_SRC_PATH = pkgs.rust.packages.stable.rustPlatform.rustLibSrc;
          shellHook = "";
        };
      });
}
