{
  description = "Node exercism";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let
          inherit (nixpkgs.lib) optional optionals;
          nodejs = pkgs.nodejs;
          check = pkgs.writeShellScriptBin "check" ''
            exec ${nodejs}/bin/npm run test
          '';
          check-watch = pkgs.writeShellScriptBin "check-watch" ''
            exec ${nodejs}/bin/npm run watch
          '';
        in pkgs.mkShell {
          buildInputs = with pkgs; [
            nodejs
            nodePackages.eslint
            yaml-language-server

            check
            check-watch
            exercism
          ];
          shellHook = "";
        };
      });
}
