const isEmpty = function (string) {
    return string.trim() === "";
};

const allCaps = function (string) {
    return string.toUpperCase() === string && string.toLowerCase() !== string;
};

const isQuestion = function (string) {
    return string.trim().slice(-1) === "?";
};

const isLoudQuestion = (string) => allCaps(string) && isQuestion(string)

export const hey = function (phrase) {
    var rules = [
        {
            test: isLoudQuestion,
            answer: "Calm down, I know what I'm doing!"
        },
        {
            test: isEmpty,
            answer: "Fine. Be that way!"
        },
        {
            test: allCaps,
            answer: "Whoa, chill out!"
        },
        {
            test: isQuestion,
            answer: "Sure."
        }
    ];

    var i, rule;
    for (i = 0; i < rules.length; i++) {
        rule = rules[i];
        if (rule.test(phrase)) {
            return rule.answer;
        }
    }

    return "Whatever.";
};
